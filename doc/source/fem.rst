FEM
===
The :mod:`soom.fem` module allows one to work with FEM (Finite Element Modelling) objects.

.. toctree::
   :maxdepth: 2

   modules/soom.fem.node
   modules/soom.fem.ebbeam
   modules/soom.fem.femmodel
   modules/soom.fem.lmass