# point_plot.py -- Test matplotlib rendering of points.
# Author: Philip Conrad, 4/4/2014 @ 3:56 PM

from soom.geometry.point import Point

# matplotlib dependencies for display:
from mpl_toolkits.mplot3d import Axes3D  #flake8: noqa
import matplotlib.pyplot as plt


if __name__ == '__main__':
    # Points to plot:
    a = Point([1, 2, 3], "point #1")
    b = Point([2, 2, 4], "point #2")
    c = Point([3, 2, 5], "point #3")
    d = Point([4, 2, 6], "point #4")
    e = Point([5, 2, 7], "point #5")
    # Generate data needed for plotting with a list-comprehension:
    point_list = [x.plot() for x in [a, b, c, d, e]]

    # Plot setup:
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # Add each point in the list of points to the scatter plot:
    for point in [point_list]:
        # pattern-match out variables:
        for (coords, color, symbol) in point:
            x = coords[0]
            y = coords[1]
            z = coords[2]
            ax.scatter(x, y, z, c=color, marker=symbol)

    # Add labels to the graph axes:
    ax.set_xlabel('X-Axis')
    ax.set_ylabel('Y-Axis')
    ax.set_zlabel('Z-Axis')

    # Render plot:
    plt.show()
