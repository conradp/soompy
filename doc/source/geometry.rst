Geometry
========
The :mod:`soom.geometry` module provides base classes for all 3D objects.

.. toctree::
   :maxdepth: 2

   modules/soom.geometry.point
   modules/soom.geometry.fline
   modules/soom.geometry.section