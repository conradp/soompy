Information for Developers
~~~~~~~~~~~~~~~~~~~~~~~~~~

This goal of this document is to provide an overview for those users that would like to create their own code that will enhance the capabilities of SOOMpy.  For example, if you would like to add a new finite element.  This document assumes that you have some experience with python and gives information about how to develop your classes to they will inter-operate with other SOOMpy classes.

Adding new FEM elements
=======================

New finite elements can be easily coded into SOOMpy.  Depending on the type of element different methods should be implemented.  For example, any element that contributes to the model stiffness should have a :meth:`globalk` method.  Any element that adds to the mass of the system should have a :meth:`globalm` method.  Finally any element that contributes to the damping matrix, such as viscous dampers should have a :meth:`globalc` method.

The FemModel performs the matrix assembly from all elements.  However, the element should aid the FemModel class perform the assembly.  This is performed by indicating the DOF in which the stiffness matrix should be assembled via the :meth:`dofList` method.

For example, consider the :class:`soom.fem.ebbeam` class.  The element uses 3 nodes.  The first two are used to indicate the ends of the beam and the third node to determine the orientation of the cross section.  The stiffness matrix returned from :meth:`soom.fem.ebbeam.globalk` is a 6x6 matrix.  The list returned from :meth:`soom.fem.ebbeam.dofList` will be of length 6.  These DOF will coincide with the global DOF in which the stiffness matrix should be assembled.

The truss element
-----------------

.. warning:: The code shown here has not been tested and it could contain errors


Consider, for example, that a truss element will be developed.  For simplicity let's consider that this element will use :class:`soom.materials.isolinear` material and will have a constant cross section defined by :class:`soom.geometry.section`.  The python file defining this class will be located in soom/fem/.  This element can inherent some properties of the :class:`soom.geometry.fline` that enables plotting and other common tasks for "linear" elements.  To inherent the properties and methods of :class:`soom.geometry.fline` use the following syntax when defining your class::

    import numpy as np
    from ..geometry.fline import Fline
    
        class Truss(Fline)

Other elements like :class:`soom.fem.ebbeam` have a :meth:`localk` method to break the formulation of the stiffness matrix in two steps.  The :meth:`localk` method builds the matrix in local coordinates and the :meth:`globalk` method transfors it to global coordinates.  The :meth:`localk` method for the trus class can be formulated as follow::

    def localk(self):
    """
    Computes the local stiffness matrix of the truss element
    
    Returns:
        k (numpy.array): Stiffness matrix in local coordinates
    """

    # Material properties
    E = self.material.e

    # Geometry properties
    A = self.section.area
    L = self.length()

    # Stiffness matrix in local coordiantes
    k = np.zeros(shape=(2,2))
    AEL = A*E/L
    k[0,0] = AEL
    k[0,1] = -AEL
    k[1,1] = AEL
    k[1,0] = -AEL

    return k

Here it is assummed that the property :attr:`material` is an instance of :class:`soom.materials.isolinear`.  Similarly, the :attr:`section` property is an instance of :class:`soom.geometry.section`.  The :meth:`length` method is inherited from the :class:`soom.geometry.fline` class.  The matrix returned is a 2x2 :class:`numpy.array`.

The next step in the implementation of this class is to write the :meth:`globalk` method.  This can be done using the following code::

    def globalk(self):
    """
    Calculates the global stiffness matrix
    
    Returns:
        K (scipy.array): Global stiffness matrix
    """        
    
    # Calculating transformation and local stiffness matrix
    T = self.tmatrix()    # Method inherited from soom.geometry.fline
    k = self.localk()

    # Re-shape the stiffness matrix to a 6x6 matrix
    np.insert(k,[1,1,2,2],0,axis = 0)
    np.insert(k,[1,1,2,2],0,axis = 1)

    # Select the appropriate parts of the transformation matrix
    T = np.delete(T,[3, 4, 5, 9, 10, 11],0)
    T = np.delete(T,[3, 4, 5, 9, 10, 11],1)

    # Calculating the stiffness matrix in global coordiantes
    K = np.dot(np.dot(T.T,k),T)

    return K

Notice that the resulting stiffness matrix is 6x6.  This could be of different sizes depending on the element.  For example, the :class:`soom.fem.ebbeam` returns a 12x12 matrix.  Another method is needed to help the :class:`soom.fem.femmodel` class know where to assemble the corresponding matrix.  This is done with the :meth:`dofList` method::

    def dofList(self):
    """
    Determines the DOF that contribute to the element's mass and stiffness
    
    Returns:
        dof (list): List of DOF
    """

    dof = range(self.nodeList[0].number*3,self.nodeList[0].number*3+3)
    dof += range(self.nodeList[1].number*3, self.nodeList[1].number*3+3)
    return dof
    
The method above will return a list corresponding to the DOF in which the matrix should be assambled.
