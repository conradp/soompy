# -*- coding: utf-8 -*-
"""
Fline
~~~~~

:synopsis: Handles 3D lines in space.
"""

import numpy as np
from ..fem.node import Node


class Fline(object):
    """
    :class:`Fline` allows the creation of lines in 3D space. Two unique
    :class:`Node` instances are required to construct such a line.

    Attributes:
        nodeList (list):   List of nodes to build from.
        comment (str):     Optional comments.
        dircosines (list): Direction cosines.
        tmatrix (matrix):  Transformation matrix.
        vhandle (list): List of objects for graphical representation
        
    """
    def __init__(self, nodeList, comment=""):
        """
        Constructor for Fline objects.

        Args:
            nodeList (list): List of nodes to build from.
            comment (str):   Optional comments.
            vhandle (list): List of objects for graphical representation
            
        """
        self.nodeList    = nodeList
        self.comment     = comment
        self.vhandle     = 0
        self.pairs       = self.buildPairs(self.nodeList)

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = "Fline:\n"
        out += "Nodes: " + str(len(self.nodeList))
        # Text wizardry to indent Points in output.
        for item in [str(x) for x in self.nodeList]:
            out += "\n- " + item.replace('\n', '\n    ')
        out += '\n'
        out += "Length = " + str(self.length()) + '\n'
        out += "Dircosines = " + str(self.dircosines()) + '\n'
        out += "Comments: " + self.comment
        return out

    def length(self):
        """
        Calculates the distance between the two nodes.

        Returns:
            number. The distance between the two nodes.
        """
        # Calculates the length of the element
        return np.sqrt(np.square(self.nodeList[0].coords[0] - self.nodeList[1].coords[0]) + 
                       np.square(self.nodeList[0].coords[1] - self.nodeList[1].coords[1]) +
                       np.square(self.nodeList[0].coords[2] - self.nodeList[1].coords[2]))

    def dircosines(self):
        out = []
        for i in range(0, 3):
            out.append( (self.nodeList[1].coords[i] - self.nodeList[0].coords[i]) / self.length() )
        return out

    def buildPairs(self, nodeList):
        """
        Legacy code. May be deprecated in future versions.
        """
        out = []
        lastNode = None
        for n in nodeList:
            out.append( (n, lastNode) )
            lastNode = n
        return out

    def display(self):
        """
        Renders this Fline instance as a string.

        Returns:
            str. The string representation of this Fline instance.
        """
        return str(self)

    def plot(self):
        """
        Plots this Fline instance via Matplotlib.
        
        The Fline.vhandle property is updated with a reference to the object
        graphically representing the Fline.

        Returns:
            None.
        """
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D

        # Set projection on current figure to 3D.
        fig = plt.gcf()
        fig.gca(projection='3d')
        
        # Plot lines between each pair of nodes.
        for item in self.nodeList:
            item.plot()
        self.vhandle = plt.plot([self.nodeList[0].coords[0], self.nodeList[1].coords[0]], #[start-x, end-x]
                 [self.nodeList[0].coords[1], self.nodeList[1].coords[1]], #[start-y, end-y]
                 [self.nodeList[0].coords[2], self.nodeList[1].coords[2]],'b') #[start-z, end-z]


    def tmatrix(self):
        """
        Provides the transformation matrix for a finite line.  The first two
        nodes define the ends of the line and the third node the orientation
        of the cross section.
        
        Returns:
            T (numpy.array): Transformatino matrix
        """
        x = self.nodeList[1].coords[0] - self.nodeList[0].coords[0]
        y = self.nodeList[1].coords[1] - self.nodeList[0].coords[1]
        z = self.nodeList[1].coords[2] - self.nodeList[0].coords[2]
        
        xr = self.nodeList[2].coords[0] - self.nodeList[0].coords[0]
        yr = self.nodeList[2].coords[1] - self.nodeList[0].coords[1]
        zr = self.nodeList[2].coords[2] - self.nodeList[0].coords[2]        
        
        alpha = np.arctan2(y,x)
        bz = np.sqrt(x**2+y**2)
        beta = np.arctan2(z,bz)
        beta = -beta
        gama =  np.arctan2( -xr*np.sin(beta)*np.cos(alpha)-yr*np.sin(beta)*np.sin(alpha)+zr*np.cos(beta),-xr*np.sin(alpha)+yr*np.cos(alpha) )
        R = np.array([[np.cos(beta)*np.cos(alpha), np.cos(beta)*np.sin(alpha), -np.sin(beta)],
                       [np.sin(gama)*np.sin(beta)*np.cos(alpha)-np.cos(gama)*np.sin(alpha), np.cos(alpha)*np.cos(gama)+np.sin(gama)*np.sin(beta)*np.sin(alpha), np.sin(gama)*np.cos(beta)],
                        [np.sin(gama)*np.sin(alpha)+np.cos(gama)*np.sin(beta)*np.cos(alpha), np.cos(gama)*np.sin(beta)*np.sin(alpha)-np.sin(gama)*np.cos(alpha), np.cos(gama)*np.cos(beta)]])
        
        T = np.zeros(shape=(12,12));
        
        T[0:3,0:3] = R;
        T[3:6,3:6] = R;
        T[6:9,6:9] = R;
        T[9:12,9:12] = R;        

        return T

# Tests __str__ method.
if __name__ == '__main__':
    a = Node([0,0,0])
    b = Node([1,2,3])

    f = Fline([a,b])

    print f

    # plotting test
    fig = plt.figure()
    ax = Axes3D(fig) 
    f.plot()
    plt.show()
