# -*- coding: utf-8 -*-
"""
Ebbeam
~~~~~~

:synopsis: Handles beam objects.


Using Ebbeam
------------

Ebbeams are used to model beams in 3D space.
To build an Ebbeam, one needs a list of Nodes, a cross-sectional area, and a
material, for example:

>>> nlist = [ Node([0,0,0]), Node([0,1,1]), Node([0,1,0]) ]
>>> s = Section(area = 0.1, j=1)
>>> mat = Isolinear(name = "Steel",e = 2e11, rho = 7850)
>>> beam = Ebbeam(nlist, s, mat)

This creates an Ebbeam from the list of provided Nodes. By convention, the
first two nodes define the beam itself, and all successive nodes are used to
compute orientation of the beam in 3D space.

Ebbeams have a graphical representation, which can be handy for visualizing
what a structure looks like. To show this representation, one can call the
:meth:`Ebbeam.plot` method of an Ebbeam. For example:

>>> beam.plot()

.. image:: images/ex_ebbeam_plot.png

Properties of an Ebbeam may change over time. The nodes a beam is constructed
from can be changed by swapping out one node for another. This can be done by
indexing into the :attr:`Ebbeam.nodelist` member variable, for example:

>>> beam.nodeList[2] = Node([1,2,3])   # changes the node used for orientation.

To see the nodes and values stored in an Ebbeam, one has to call its
:meth:`Ebbeam.__str__` method. The easiest way to do this is to just ``print``
the Ebbeam in question (``print`` calls :meth:`Ebbeam.__str__` behind the
scenes). For example:

>>> print beam
Ebbeam:
Nodes: 3
- Node:
    X = 0
    Y = 0
    Z = 0
    Deformation = [0, 0, 0, 0, 0, 0]
    Cdof = [0, 0, 0, 0, 0, 0]
    Comments:
- Node:
    X = 0
    Y = 1
    Z = 1
    Deformation = [0, 0, 0, 0, 0, 0]
    Cdof = [0, 0, 0, 0, 0, 0]
    Comments:
- Node:
    X = 1
    Y = 2
    Z = 3
    Deformation = [0, 0, 0, 0, 0, 0]
    Cdof = [0, 0, 0, 0, 0, 0]
    Comments:
Length = Not Implemented
Dircosines = Not Implemented
Section = Not Implemented
Material = Not Implemented
T = Not Implemented
K = Not Implemented
M = Not Implemented
Comments:

"""

import numpy as np
import scipy.sparse as sp

from .node import Node
from ..geometry.fline import Fline
from ..materials.isolinear import Isolinear
from ..geometry.section import Section


class Ebbeam(Fline):
    """
    :class:`Ebbeam` is used to model beams in structures.

    Attributes:
        nodeList (list): List of nodes to build from.
        section (Section): Cross-section object.
        material (Isolinear): Material properties of the beam.
        comment (str): Optional comments.
    """
    def __init__(self,
                 nodeList,
                 section,
                 material,
                 comment=""):
        """
        Constructor for Ebbeam objects.

        Args:
            nodeList (list): List of nodes to build from.
            section (Section): Cross-section object.
            material (Isolinear): Material properties of the beam.
            comment (str): Optional comments.
        """
        super(Ebbeam, self).__init__(nodeList=nodeList, comment=comment)
        self.section  = section
        self.material = material

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = "Ebbeam:\n"
        out += "Nodes: " + str(len(self.nodeList))
        # Text wizardry to indent Points in output.
        for item in [str(x) for x in self.nodeList]:
            out += "\n- " + item.replace('\n', '\n    ')
        out += '\n'
        out += "Length = " + "Not Implemented" + '\n'
        out += "Dircosines = " + "Not Implemented" + '\n'
        out += "Section = " + "Not Implemented" + '\n'
        out += "Material = " + "Not Implemented" + '\n'
        out += "T = " + "Not Implemented" + '\n'
        out += "K = " + "Not Implemented" + '\n'
        out += "M = " + "Not Implemented" + '\n'
        out += "Comments: " + self.comment
        return out

    def display(self):
        """
        Renders this Ebbeam instance as a string.

        Returns:
            str. The string representation of this Ebbeam instance.
        """
        return str(self)

    def localm(self):
        """
        Computes the local mass matrix of the beam.

        Returns:
            m (numpy.array): The local mass matrix.
        """
        # Material properties
        Rho = self.material.rho

        # Geometry properties
        Ar = self.section.area
        J = self.section.j

        # Lenght property
        L = self.length()

        # some additional terms
        gama = Rho*Ar
        I    = np.eye(12, 12)
        m    = np.zeros(shape=(12, 12))

        m[0, 0]   =     gama*L/3
        m[0, 6]   =     gama*L/6
        m[1, 1]   =  13*gama*L/35
        m[1, 5]   =  11*gama*L**2/210
        m[1, 7]   =   9*gama*L/70
        m[1, 11]  = -13*gama*L**2/420
        m[2, 2]   =  13*gama*L/35
        m[2, 4]   = -11*gama*L**2/210
        m[2, 8]   =   9*gama*L/70
        m[2, 10]  =  13*gama*L**2/420
        m[3, 3]   =    Rho*J*L/3
        m[3, 9]   =    Rho*J*L/6
        m[4, 4]   =     gama*L**3/105
        m[4, 8]   = -13*gama*L**2/420
        m[4, 10]  =    -gama*L**3/140
        m[5, 5]   =     gama*L**3/105
        m[5, 7]   =  13*gama*L**2/420
        m[5, 11]  =    -gama*L**3/140
        m[6, 6]   =     gama*L/3
        m[7, 7]   =  13*gama*L/35
        m[7, 11]  = -11*gama*L**2/210
        m[8, 8]   =  13*gama*L/35
        m[8, 10]  =  11*gama*L**2/210
        m[9, 9]   =    Rho*J*L/3
        m[10, 10] =     gama*L**3/105
        m[11, 11] =     gama*L**3/105
        m        = m+m.transpose()-np.multiply(I, m)
        return m

    def localk(self):
        """
        Computes the local stiffnes matrix of the beam.

        Returns:
            k (numpy.array): The local stiffness matrix.
        """
        # Material properties
        E = self.material.e
        G = self.material.g

        # Geometry properties
        Ar  = self.section.area
        i2  = self.section.iy
        i3  = self.section.iz
        i1  = self.section.j
        sa2 = self.section.asy
        sa3 = self.section.asz

        # Lenght property
        L = self.length()

        # some additional terms
        try:
            c2 = 3*E*i2/(G*sa2*L**2.0)
        except ZeroDivisionError:
            c2 = float('nan')

        try:
            c3 = 3*E*i3/(G*sa3*L**2)
        except ZeroDivisionError:
            c3 = float('nan')
        I = np.eye(12, 12)

        # c2  = 3*E*i2/(G*sa2*L**2);
        # c3  = 3*E*i3/(G*sa3*L**2);

        # Matrix elements
        A   = E*Ar/L
        T   = G*i1/L
        Q2  = (12*E*i3/L**3)*(1/(1+4*c3))
        Q3  = (12*E*i2/L**3)*(1/(1+4*c2))
        Q2m = (6*E*i3/L**2)*(1/(1+4*c3))
        Q3m = (6*E*i2/L**2)*(1/(1+4*c2))
        M2  = (4*E*i2/L)*((1+c2)/(1+4*c2))
        M3  = (4*E*i3/L)*((1+c3)/(1+4*c3))
        M2t = (2*E*i2/L)*((1-2*c2)/(1+4*c2))
        M3t = (2*E*i3/L)*((1-2*c3)/(1+4*c3))

        # k = np.matrix([[E*A/L, -E*A/L], [-E*A/L, E*A/L]])
        k = np.zeros(shape=(12, 12))

        k[0, 0]   = A
        k[0, 6]   = -A
        k[1, 1]   = Q2
        k[1, 5]   = Q2m
        k[1, 7]   = -Q2
        k[1, 11]  = Q2m
        k[2, 2]   = Q3
        k[2, 4]   = -Q3m
        k[2, 8]   = -Q3
        k[2, 10]  = -Q3m
        k[3, 3]   = T
        k[3, 9]   = -T
        k[4, 4]   = M2
        k[4, 8]   = Q3m
        k[4, 10]  = M2t
        k[5, 5]   = M3
        k[5, 7]   = -Q2m
        k[5, 11]  = M3t
        k[6, 6]   = A
        k[7, 7]   = Q2
        k[7, 11]  = -Q2m
        k[8, 8]   = Q3
        k[8, 10]  = Q3m
        k[9, 9]   = T
        k[9, 10]  = M2
        k[11, 11] = M3
        k         = k+k.transpose()-np.multiply(I, k)

        return k

    def globalm(self):
        """
        Calculates the global mass matrix

        Args:
            femm (scipy.sparse.lil): Mass matrix of the complete FEM model

        Returns:
            M (numpy.array): Global mass matrix.
        """
        # Calculating transformation and local mass matrix
        T = self.tmatrix()
        m = self.localm()

        # Calculate global mass matrix
        M = np.dot(np.dot(T.T, m), T)

        return M

    def globalk(self):
        """
        Calculates the global stiffness matrix


        Returns:
            K (scipy.array): Global stiffness matrix
        """
        # Calculating transformation and local stiffness matrix
        T = self.tmatrix()
        k = self.localk()

        # Calculate global stiffness matrix
        K = np.dot(np.dot(T.T, k), T)

        return K

    def dofList(self):
        """
        Determines the DOF that contribute to the element's mass and stiffness

        Returns:
            dof (list): List of DOF
        """
        dof = range(self.nodeList[0].number*6, self.nodeList[0].number*6+6)
        dof += range(self.nodeList[1].number*6, self.nodeList[1].number*6+6)
        return dof

    def check(self):
        """
        Checks if the arguments of the ebeeam have been set correctly

        The arguments that are checked are:

        * nodeList is a list of 3 elements of type Node
        * section is of type section

        Returns:
            checkFlag (boolean): True if the check passes
        """
        checkFlag = True

        # Check that the nodeList is a list of 3 nodes
        if type(self.nodeList) is not list:
            print "The ebbeam.nodeList is expected to be a list (id %i)" % id(self)
            checkFlag = False
        elif len(self.nodeList) is not 3:
            print "The nodeList is expected to have 3 elements (id %i)" % id(self)
            checkFlag = False
        elif not all([isinstance(item, Node) for item in self.nodeList]):
            print "The elements of ebbeam.nodeList are expected to be Nodes (id %i)" % id(self)
            checkFlag = False

        # Checking that the section is type section
        if not isinstance(self.section, Section):
            print "The ebbeam.section is expected to be type section (id %i)" % id(self)
            checkFlag = False

        return checkFlag
