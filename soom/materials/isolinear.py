# -*- coding: utf-8 -*-
"""
Isolinear
~~~~~~~~~

:synopsis: Handles isolinear materials.


Using Isolinear Materials
-------------------------

Isolinear allow one to model isolinear materials in SOOMpy. An Isolinear
material can be created like so:

>>> m = Isolinear(name="Steel", e=2e11)

This creates a Isolinear material named "Steel", with an elasticity of 2e11
units.

Unfortunately, Isolinear doesn't have a graphical representation at this time.
This means that calling :meth:`Isolinear.plot` does nothing.

To change an Isolinear's properties, just reassign any of its public member
variables. For example:

>>> m.name = "Concrete"     # Isolinear m now is named "Concrete".
>>> m.e = 4.35e6            # m now has an elasticity of 4.35e6.

To see the values stored in a Isolinear, one has to call its
:meth:`Isolinear.__str__` method. The easiest way to do this is to just
``print`` the Isolinear in question (``print`` calls :meth:`Isolinear.__str__`
behind the scenes). For example:

>>> print m
Material Name = Concrete
Type = isolinear
rho  = 0
e    = 4350000.0
nu   = 0
g    = 2175000.0

"""


class Isolinear:
    """
    :class:`Isolinear` allows the creation of isolinear materials.

    Note:
        At this time, dependent properties are set when the object is created.
        In the future, this may be replaced by methods that compute the
        dependent properties on-demand.

    Warning:
        Some methods, namely :meth:`display`, and :meth:`plot` are
        not implemented yet.

    Attributes:
        name (str):  The display name for this material.
        rho (float): Rho of this material.
        e (float):   Elasticity of this material.
        nu (float):  Nu of this material.
        g (number):  G.
    """
    def __init__(self, name="unknown", rho=0.0, e=0.0, nu=0.0):
        """
        Constructor for Isolinear objects.

        Args:
            name (str):  The display name for this material.
            rho (float): Rho of this material.
            e (float):   Elasticity of this material.
            nu (float):  Nu of this material.

        Examples:
            >>> from soom.materials.isolinear import Isolinear
            >>> Isolinear("Steel", e=2e11) # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
            <soom.materials.isolinear.Isolinear instance at 0x...>

        """
        self.name = name
        self.rho  = rho
        self.e    = e
        self.nu   = nu
        self.g    = e / (2 * (nu + 1))  # dependent property

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.

        Examples:
            >>> from soom.materials.isolinear import Isolinear
            >>> print Isolinear("Steel", e=2e11) # doctest: +NORMALIZE_WHITESPACE
            Material Name = Steel
            Type = isolinear
            rho  = 0
            e    = 2e+11
            nu   = 0
            g    = 1e+11
            <BLANKLINE>
        """
        out = ""
        out += "Material Name = " + self.name  + '\n'
        out += "Type = isolinear\n"
        out += "rho  = " + str(self.rho) + '\n'
        out += "e    = " + str(self.e) + '\n'
        out += "nu   = " + str(self.nu) + '\n'
        out += "g    = " + str(self.g) + '\n'
        return out

    def check(self):
        flagCheck = True

        # Check that rho, e and nu are float
        for prop in ["rho","e","nu"]:
            if type(getattr(self,prop)) is not float:
                flagCheck = False
                print "The isolinear.%s is expected to be a double (id %i)" %(prop,id(self))

        return flagCheck

    def display(self):
        """
        Not implemented.
        """
        pass

    def plot(self):
        """
        Not implemented.
        """
        pass
